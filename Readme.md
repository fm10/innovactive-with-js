## Innovactive

# Resumen

Innovactive es una aplicación para los procesos de inovacción dentro de las organizaciones,
permitiendo crear ideas los cuales cumplen con un reto, estas ideas se le permite tener un seguimiento,
comentar, dar me gusta, revizarlas y pasar por un comite de aprobación para ejecutar la idea.

# Tecnologias

- Ionic con Angular.
- Fastify.
- MongoDB.

# Copyright

Copyright © 2023 - Imaginario® todos los derechos reservados - Colombia.
