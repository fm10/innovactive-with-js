import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('../home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'retos',
        loadChildren: () =>
          import('../retos/retos.module').then((m) => m.RetosPageModule),
      },
      {
        path: 'ideas',
        loadChildren: () =>
          import('../mis-ideas/ideas.module').then((m) => m.IdeasPageModule),
      },
      {
        path: 'postulacion',
        loadChildren: () =>
          import('../postulacion/postulacion.module').then(
            (m) => m.PostulacionPageModule
          ),
      },
      {
        path: '',
        redirectTo: '/colaborador/home',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/colaborador/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
