import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RetosPage } from './retos.page';

import { RetosPageRoutingModule } from './retos-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RetosPageRoutingModule],
  declarations: [RetosPage],
})
export class RetosPageModule {}
