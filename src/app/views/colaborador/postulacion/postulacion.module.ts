import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { PostulacionPage } from './postulacion.page';

import { PostulacionPageRoutingModule } from './postulacion-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostulacionPageRoutingModule,
  ],
  declarations: [PostulacionPage],
})
export class PostulacionPageModule {}
