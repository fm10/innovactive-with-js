import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostulacionPage } from './postulacion.page';

describe('PostulacionPage', () => {
  let component: PostulacionPage;
  let fixture: ComponentFixture<PostulacionPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PostulacionPage],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(PostulacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
