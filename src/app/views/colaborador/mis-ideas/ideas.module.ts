import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { IdeasPage } from './ideas.page';

import { IdeasPageRoutingModule } from './ideas-routing.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, IdeasPageRoutingModule],
  declarations: [IdeasPage],
})
export class IdeasPageModule {}
