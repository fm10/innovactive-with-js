import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./views/Login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'colaborador',
    loadChildren: () =>
      import('./views/colaborador/tabs/tabs.module').then(
        (m) => m.TabsPageModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
