import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'innovactive.with.js',
  appName: 'innovactive-with-js',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
